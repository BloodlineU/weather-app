package com.example.weatherapp

import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Path

interface WeatherService {

    @GET("data/{api_version}/weather")
    fun getWeather(
        @Path("api_version") version: String = "2.5",
        @Query("q") city: String,
        @Query("appId") appId: String = "e60fc6752c621242b66596b4ccabf2f2"
    ) : Call<WeatherResponse>


    companion object {
        val instance: WeatherService by lazy {
            val retrofit = Retrofit.Builder()
                .baseUrl("https://api.openweathermap.org")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            retrofit.create<WeatherService>(WeatherService::class.java)
        }
    }
}